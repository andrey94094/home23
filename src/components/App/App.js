import { Fragment } from 'react';
import './App.scss';
import Header from '../Header/Header'
import Footer from '../Footer/Footer'
import {Circle_Color} from './constant';
import Circle, { del } from '../Circle/Circle';
import Count from '../Count/Count';


export default function App() {
  return (
    <Fragment>
      <Count text = 'Кнопка-1'/>
      <Header />
      <main>{Circle_Color.map((item)=>(<Circle{...item}/>))}
      </main>
      <Count text = 'Кнопка-2'/>
      <Footer/>
    </Fragment>
  );
}

// export default App;
